-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2016 at 09:09 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `revolution`
--

-- --------------------------------------------------------

--
-- Table structure for table `re_app_config`
--

CREATE TABLE IF NOT EXISTS `re_app_config` (
  `_id` int(11) NOT NULL,
  `app_name` int(11) NOT NULL,
  `app_version_build` int(11) NOT NULL,
  `app_version` int(11) NOT NULL,
  `app_team` int(11) NOT NULL,
  `date_created` int(11) NOT NULL,
  `date_modified` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `re_report_bug`
--

CREATE TABLE IF NOT EXISTS `re_report_bug` (
  `_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL,
  `type` int(11) NOT NULL,
  `note` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `re_task_add`
--

CREATE TABLE IF NOT EXISTS `re_task_add` (
  `_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `type` int(11) NOT NULL,
  `note` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `re_users`
--

CREATE TABLE IF NOT EXISTS `re_users` (
  `_id` int(11) NOT NULL,
  `Firstname` varchar(50) NOT NULL,
  `Lastname` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `gender` varchar(2) NOT NULL,
  `level` int(3) NOT NULL,
  `department` int(3) NOT NULL,
  `college` int(3) NOT NULL,
  `online_status` int(3) NOT NULL,
  `last_login` datetime NOT NULL,
  `login_count` datetime NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `matric_number` varchar(9) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `current_location` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `re_user_config`
--

CREATE TABLE IF NOT EXISTS `re_user_config` (
  `_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `app_on_startup` int(11) NOT NULL,
  `app_on_background` int(11) NOT NULL,
  `send_crashes` int(11) NOT NULL,
  `show_meetings` int(11) NOT NULL,
  `auto_wake` int(11) NOT NULL,
  `backup_messages` int(11) NOT NULL,
  `detect_location` int(11) NOT NULL,
  `keep_offline` int(11) NOT NULL,
  `use_dark` int(11) NOT NULL,
  `auto_iupdate` int(11) NOT NULL,
  `date_created` int(11) NOT NULL,
  `date_modified` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `re_user_notification`
--

CREATE TABLE IF NOT EXISTS `re_user_notification` (
  `_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notify_type` int(11) NOT NULL,
  `notify_title` varchar(50) NOT NULL,
  `notify_note` varchar(255) NOT NULL,
  `notify_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `from_user_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `re_report_bug`
--
ALTER TABLE `re_report_bug`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `re_users`
--
ALTER TABLE `re_users`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `re_user_config`
--
ALTER TABLE `re_user_config`
  ADD PRIMARY KEY (`_id`), ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `re_user_notification`
--
ALTER TABLE `re_user_notification`
  ADD PRIMARY KEY (`_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `re_users`
--
ALTER TABLE `re_users`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
